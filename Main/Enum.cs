﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Zodiac
{
    public enum GameState
    {
        NONE,
        INITIALIZE,
        MAIN_MENU,
        GAMEPLAY,
        COUNT = GAMEPLAY + 1,
        IGNORE
    }

    public enum ChangeType
    {
        SET,
        PUSH,
    }
}
