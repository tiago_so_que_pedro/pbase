﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Zodiac
{
    public class LevelData : DataContainer
    {
        public LevelInfo[] Levels;
    }
}