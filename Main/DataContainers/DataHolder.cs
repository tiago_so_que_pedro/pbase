using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zodiac.Debug;

namespace Zodiac
{
    [System.Serializable]
    public class DataHolder
    {
        public DataContainer[] Datas => _datas;

        [SerializeField]
        private DataContainer[] _datas;

        ///<summary>
        ///Intializes all data in the game
        ///</summary>
        public void Initialize()
        {
            for (int i = 0; i < _datas.Length; i++)
            {
                var data = _datas[i];
                data.Initialize(data.name);
            }
        }

        ///<summary>
        ///Returns a DataContainer object from the DataContainers collection
        ///</summary>
        public DataContainer GetData<T>() where T : DataContainer
        {
            var type = typeof(T);

            foreach (DataContainer data in _datas)
            {
                if (data is T)
                {
                    return data as T;
                }
            }

            Log.Error("DataHolder", string.Format("Couldn't find any data of type {0}", type.Name));

            return null;
        }
    }
}
