﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zodiac.Debug;

namespace Zodiac
{
    public class SimulationData : DataContainer
    {

        #region Getters

        public GameState CurrentState
        {
            get
            {
                if (_states != null && _states.Count > 0)
                {
                    return _states[_states.Count - 1];
                }

                return GameState.IGNORE;
            }
        }

        #endregion

        [SerializeField]
        private List<GameState> _states;

        [Header("Change State")]
        [SerializeField]
        public List<ChangeStateSettings> _changeStateActions;

        public override void Initialize(string name)
        {
            base.Initialize(name);

            _states = new List<GameState>();
            _changeStateActions = new List<ChangeStateSettings>();
        }

        public override void Update()
        {
            base.Update();

            ManageStateChanging();
        }

        public void ChangeState(ChangeType changeType, GameState toState)
        {
            _changeStateActions.Add(new ChangeStateSettings(toState, changeType));
        }

        private void ManageStateChanging()
        {
            if (_changeStateActions.Count > 0)
            {
                GameState toState = _changeStateActions[0].GameState;
                ChangeType changeType = _changeStateActions[0].ChangeType;

                switch (changeType)
                {
                    case ChangeType.PUSH:
                        {
                            _states.Add(toState);
                            Log(string.Format("Pushing state {0}", toState));
                        }
                        break;
                    case ChangeType.SET:
                        {
                            _states[_states.Count - 1] = toState;
                            Log(string.Format("Setting state {0}", toState));
                        }
                        break;
                }

                _changeStateActions.RemoveAt(0);
            }
        }
    }
}