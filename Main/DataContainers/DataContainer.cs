﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zodiac.Debug;

namespace Zodiac
{
    public class DataContainer : MonoBehaviour
    {
        #region UNITY_EDITOR
        [SerializeField]
        private bool _debug;
        public void Log(string message)
        {
#if UNITY_EDITOR
            if (!_debug)
            {
                return;
            }

            Zodiac.Debug.Log.Message(name, message);
#endif
        }

        #endregion

        public string Name => _name;

        private string _name;
        [SerializeField]
        private GameState[] _targetStates;

        public virtual void Initialize(string name)
        {
            _name = name;
            Log(StringConstants.Initialized);
        }

        public virtual void Update()
        {
            if (!CanBeUpdated())
            {
                return;
            }
        }

        public bool CanBeUpdated()
        {
            for (int i = 0; i < _targetStates.Length; i++)
            {
                if (_targetStates[i] == Game.Instance.CurrentState || _targetStates[i] == GameState.COUNT)
                {
                    return true;
                }
            }

            return false;
        }
    }
}