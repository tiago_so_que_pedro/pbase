﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Zodiac
{
    [System.Serializable]
    public class LevelInfo
    {
        public Vector3 PlayerStartPosition { get { return _playerPosition; } set { _playerPosition = value; } }
        public string SceneRef { get { return _sceneRef; } set { _sceneRef = value; } }

        [SerializeField]
        private Vector3 _playerPosition;

        [SerializeField, SceneField]
        private string _sceneRef;

        public LevelInfo()
        {
            _playerPosition = new Vector3();
        }
    }
}