﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Zodiac
{
    public class ChangeStateSettings
    {
        public ChangeType ChangeType => _changeType;
        public GameState GameState => _gameState;

        private ChangeType _changeType;
        private GameState _gameState;

        public ChangeStateSettings(GameState gameState, ChangeType changeType)
        {
            _gameState = gameState;
            _changeType = changeType;
        }
    }
}
