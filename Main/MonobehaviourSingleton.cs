using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Zodiac
{
    public class MonobehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        
        public static T Instance;

        void Awake()
        {
            AddInstance();
            AddDontDestroyOnLoad();
        }

        void AddInstance()
        {
            if (Instance != null)
            {
                GameObject.Destroy(GameObject.Find(Instance.name));
                Instance = null;
            }

            Instance = this as T;
        }

        void AddDontDestroyOnLoad()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}