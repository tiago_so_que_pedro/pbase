﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zodiac.Debug;

namespace Zodiac
{
    public class Game : MonobehaviourSingleton<Game>
    {
        #region Getters
        public DataHolder DataHolder => _dataHolder;
        public GameState CurrentState
        {
            get
            {
                SimulationData simulationData = (SimulationData)DataHolder.GetData<SimulationData>();

                if (simulationData != null)
                {
                    return simulationData.CurrentState;
                }

                return GameState.NONE;
            }
        }
        #endregion

        [SerializeField]
        private DataHolder _dataHolder;

        #region Unity Functions

        void Start()
        {
            _dataHolder.Initialize();

            SimulationData _simulationData = (SimulationData)DataHolder.GetData<SimulationData>();

            _simulationData.ChangeState(ChangeType.PUSH, GameState.INITIALIZE);
        }

        void Update()
        {
            SimulationData _simulationData = (SimulationData)DataHolder.GetData<SimulationData>();
            PlayerData _playerData = (PlayerData)DataHolder.GetData<PlayerData>();

            _simulationData.Update();
            _playerData.Update();
        }
        #endregion
    }
}