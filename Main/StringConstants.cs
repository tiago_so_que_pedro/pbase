﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Zodiac
{
    public static class StringConstants
    {
        public const string Initialized = "Initialized";
        public const string SimulationData_NullOrEmptyStatesList = "The states list is empty or null";
    }
}
