﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Zodiac.Debug
{
    public static class Log
    {
        public static void Message(string owner, string message)
        {
            string finalMessage = string.Format("<color=yellow>[{0}]</color>{1}", owner, message);
            UnityEngine.Debug.Log(finalMessage);
        }

        public static void Error(string owner, string message)
        {   
            string finalMessage = string.Format("<color=red>[{0}]</color>{1}", owner, message);
            UnityEngine.Debug.Log(finalMessage);
        }
    }
}
