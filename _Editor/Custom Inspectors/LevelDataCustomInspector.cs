﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System;

namespace Zodiac.Editor
{

    class LevelInfoEditor
    {
        public LevelInfo LevelInfo;
        public bool IsSettingPlayerPosition;
    }

    [CustomEditor(typeof(LevelData), true)]
    public class LevelDataCustomInspector : DataContainerCustomInspector
    {

        bool foldDataContainerValues = false;
        bool foldLevels = false;
        static LevelInfoEditor[] settingPlayerPosData;

        void OnEnable()
        {
            LevelData levelData = target as LevelData;

            if (levelData != null)
            {
                settingPlayerPosData = new LevelInfoEditor[levelData.Levels.Length];

                int id = 0;
                foreach (var levelInfo in levelData.Levels)
                {
                    settingPlayerPosData[id] = new LevelInfoEditor();
                    settingPlayerPosData[id].LevelInfo = levelInfo;
                    settingPlayerPosData[id].IsSettingPlayerPosition = false;
                    id++;
                }
            }
        }

        public override void OnInspectorGUI()
        {
            OverrideWithoutBase();

            int id = 0;
            foreach (var levelInfo in settingPlayerPosData)
            {
                GUILayout.Label(settingPlayerPosData[id].IsSettingPlayerPosition.ToString());
                id++;
            }

            DrawFoldableContent("Data Container values", () => Content_DataContainerValues(), ref foldDataContainerValues);
            DrawFoldableContent("Scenes", () => Content_LevelInfo(), ref foldLevels);
        }

        private void Content_DataContainerValues()
        {
            SerializedObject serializedObject = new SerializedObject(target as LevelData);

            SerializedProperty _targetStates = serializedObject.FindProperty("_targetStates");

            EditorGUILayout.PropertyField(_targetStates);

            serializedObject.ApplyModifiedProperties();
        }

        private void Content_LevelInfo()
        {
            LevelData levelData = target as LevelData;

            if (levelData == null)
            {
                return;
            }

            if (levelData.Levels == null)
            {
                levelData.Levels = new LevelInfo[0];
            }

            GUILayout.BeginHorizontal();
            {
                GUILayout.Label("Add Scene");
                if (GUILayout.Button("+", GUILayout.Width(20)))
                {
                    LevelInfo newLevelInfo = new LevelInfo();
                    ArrayUtility.Add(ref levelData.Levels, newLevelInfo);
                    ArrayUtility.Add(ref settingPlayerPosData, new LevelInfoEditor() { LevelInfo = newLevelInfo, IsSettingPlayerPosition = false });
                }
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.BeginVertical();
            {
                foreach (var data in settingPlayerPosData)
                {
                    LevelInfoDrawner(data);
                }
            }
            GUILayout.EndVertical();
        }

        private void LevelInfoDrawner(LevelInfoEditor data)
        {

            LevelData levelData = target as LevelData;

            GUIStyle guiStyle_background = new GUIStyle();
            guiStyle_background.normal.background = new Texture2D(1, 1);
            guiStyle_background.normal.background.SetPixel(0, 0, new Color(15 / 255f, 15 / 255f, 15 / 255f));
            guiStyle_background.normal.background.Apply();

            GUILayout.BeginVertical(guiStyle_background);
            {

                if (GUILayout.Button("x", GUILayout.Width(20)))
                {
                    ArrayUtility.Remove(ref settingPlayerPosData, data);
                    ArrayUtility.Remove(ref levelData.Levels, data.LevelInfo);
                }

                var oldScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(data.LevelInfo.SceneRef);

                EditorGUI.BeginChangeCheck();

                GUILayout.BeginHorizontal();
                {
                    var newScene = EditorGUILayout.ObjectField("Scene Ref", oldScene, typeof(SceneAsset), false) as SceneAsset;

                    if (!string.IsNullOrEmpty(data.LevelInfo.SceneRef) && EditorSceneManager.GetActiveScene().name != System.IO.Path.GetFileNameWithoutExtension(data.LevelInfo.SceneRef))
                    {
                        if (GUILayout.Button("Load"))
                        {
                            EditorSceneManager.OpenScene(data.LevelInfo.SceneRef);
                        }
                    }

                    if (EditorGUI.EndChangeCheck())
                    {
                        var newPath = AssetDatabase.GetAssetPath(newScene);
                        data.LevelInfo.SceneRef = newPath;
                        EditorUtility.SetDirty(levelData);
                    }
                }
                GUILayout.EndHorizontal();

                GUILayout.BeginHorizontal();
                {
                    if (GUILayout.Button(data.IsSettingPlayerPosition ? "●" : "○", GUILayout.Width(20)))
                    {
                        data.IsSettingPlayerPosition = data.IsSettingPlayerPosition ? false : true;
                    }

                    GUILayout.Label(data.IsSettingPlayerPosition ? "XYZ" : "Set Player Position");
                }
                GUILayout.EndHorizontal();
            }
            GUILayout.EndVertical();

            GUILayout.Space(5f);
        }

        void OnSceneGUI()
        {
            LevelData levelData = target as LevelData;

            if (levelData == null)
            {
                UnityEngine.Debug.Log("Exit OnSceneGUI !");
                return;
            }

            for (int id = 0; id < levelData.Levels.Length; id++)
            {
                var data = settingPlayerPosData[id];
                var levelInfo = data.LevelInfo;

                if (data.IsSettingPlayerPosition)
                {
                    UnityEngine.Debug.Log("Draw Handler OnSceneGUI !");
                    Vector3 lastPos = Handles.PositionHandle(levelInfo.PlayerStartPosition, Quaternion.identity);
                    if (lastPos != levelInfo.PlayerStartPosition)
                    {
                        levelInfo.PlayerStartPosition = lastPos;
                        EditorUtility.SetDirty(levelData);
                    }
                    break;
                }
            }

            GUIStyle style = new GUIStyle();
            style.normal.textColor = Color.green;

            foreach (var data in settingPlayerPosData)
            {
                if (EditorSceneManager.GetActiveScene().name == System.IO.Path.GetFileNameWithoutExtension(data.LevelInfo.SceneRef))
                {
                    Handles.Label(data.LevelInfo.PlayerStartPosition, "Playey's position", style);
                    break;
                }
            }
        }
    }
}