﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Zodiac.Editor
{
    [CustomEditor(typeof(DataContainer), true)]
    public class DataContainerCustomInspector : UnityEditor.Editor
    {
        GUIStyle greenFont = new GUIStyle();
        GUIStyle redFont = new GUIStyle();

        public override void OnInspectorGUI()
        {
            DataContainer dataContainer = (DataContainer)target;

            InitializeStyles();

            if (dataContainer != null)
            {
                if (Application.isPlaying && Application.isEditor)
                {
                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.Label("Updating: ");
                        GUILayout.Label(dataContainer.CanBeUpdated().ToString(), (dataContainer.CanBeUpdated()) ? greenFont : redFont);
                    }
                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();
                }
            }

            base.OnInspectorGUI();
        }

        protected void OverrideWithoutBase()
        {
            DataContainer dataContainer = (DataContainer)target;

            InitializeStyles();

            if (dataContainer != null)
            {
                if (Application.isPlaying && Application.isEditor)
                {
                    GUILayout.BeginHorizontal();
                    {
                        GUILayout.Label("Updating: ");
                        GUILayout.Label(dataContainer.CanBeUpdated().ToString(), (dataContainer.CanBeUpdated()) ? greenFont : redFont);
                    }
                    GUILayout.FlexibleSpace();
                    GUILayout.EndHorizontal();
                }
            }
        }

        protected void DrawFoldableContent(string title, Action drawn, ref bool folded)
        {
            GUIStyle guiStyle_background = new GUIStyle();
            guiStyle_background.normal.background = new Texture2D(1, 1);
            guiStyle_background.normal.background.SetPixel(0, 0, new Color(35 / 255f, 35 / 255f, 35 / 255f));
            guiStyle_background.normal.background.Apply();

            GUIStyle guiStyle_padding = new GUIStyle();
            guiStyle_padding.padding = new RectOffset(15, 0, 0, 0);
            guiStyle_padding.normal.textColor = Color.white;

            GUILayout.BeginVertical(guiStyle_background);
            {
                GUILayout.BeginHorizontal();
                {
                    if (GUILayout.Button(folded == true ? "►" : "▼", GUILayout.Width(25)))
                    {
                        folded = !folded;
                    }
                    GUILayout.Label(title);
                }
                GUILayout.FlexibleSpace();
                GUILayout.EndHorizontal();

                if (!folded)
                {
                    GUILayout.BeginVertical(guiStyle_padding);
                    {
                        drawn?.Invoke();
                    }
                    GUILayout.EndVertical();
                }
            }
            GUILayout.EndVertical();

            GUILayout.Space(10f);
        }

        void InitializeStyles()
        {
            greenFont.normal.textColor = Color.green;
            redFont.normal.textColor = Color.red;
        }
    }
}